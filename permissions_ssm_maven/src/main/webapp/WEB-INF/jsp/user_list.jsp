<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%  
	response.setHeader("Cache-Control","no-store");  
	response.setHeader("Pragrma","no-cache");  
	response.setDateHeader("Expires",0);  
%> 
<!DOCTYPE html>
<html>


<!-- Mirrored from www.zi-han.net/theme/hplus/table_basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:20:01 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>H+ 后台主题UI框架 - 基础表格</title>
    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

    <link href="${pageContext.request.contextPath}/hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/hplus/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/hplus/css/animate.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/hplus/css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">


</head>

<body class="gray-bg">

	<div class="wrapper wrapper-content animated fadeIn">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>基本面板 <small class="m-l-sm">这是一个自定义面板</small></h5>
				<div class="ibox-tools">
					<a class="collapse-link">
					<i class="fa fa-chevron-up"></i>
					</a>
					<a class="dropdown-toggle" data-toggle="dropdown" href="tabs_panels.html#">
					<i class="fa fa-wrench"></i>
					</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="tabs_panels.html#">选项1</a>
						</li>
						<li><a href="tabs_panels.html#">选项2</a>
						</li>
					</ul>
					<a class="close-link">
					<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			
			<!-- 下半部分 -->
			<div class="ibox-content" style="display: block;">
				<!-- Example Events -->
				<div class="example-wrap">
						<div class="bootstrap-table">
							<!-- 表格 -->
							<table id="user_list_table"></table>
						</div>
						<div class="clearfix">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
    <script src="${pageContext.request.contextPath}/hplus/js/jquery.min.js?v=2.1.4"></script>
    <script src="${pageContext.request.contextPath}/hplus/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="${pageContext.request.contextPath}/hplus/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="${pageContext.request.contextPath}/hplus/js/content.min.js?v=1.0.0"></script>
    <script src="${pageContext.request.contextPath}/hplus/js/plugins/iCheck/icheck.min.js"></script>
    
    <script src="${pageContext.request.contextPath}/hplus/js/plugins/bootstrap-table/bootstrap-table.min.js"></script>
    <script src="${pageContext.request.contextPath}/hplus/js/plugins/bootstrap-table/bootstrap-table-mobile.min.js"></script>
    <script src="${pageContext.request.contextPath}/hplus/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
    
	<script>
		
		// 初始化表格
		$('#user_list_table').bootstrapTable({
		    url: '${pageContext.request.contextPath}/user/list',
		    toolbar : "#fixed-table-toolbar",
		    method : 'get',	// 使用 post 后台无法接受值
		    sidePagination: "server", // 服务端处理分页  
		    cache : false,	 // 禁止 ajax 缓存
		    pagination: true, // 开启分页功能
		    sortName : '0',	// 排线列
		    showColumns: "true",// 是否显示 内容列下拉框  
		    striped: true,// 设置为 true 会有隔行变色效果  
		    queryParams: function (params) {// 自定义参数，这里的参数是传给后台的，我这是是分页用的  
		        return {// 这里的 params 是 table 提供的  
		        	start: params.offset, // 从数据库第几条记录开始  
		        	count: params.limit // 找多少条  
		        };  
		    },  
		    showRefresh : true,
		    pageSize: 5, // 如果设置了分页，页面数据条数  
		    iconSize: "outline",
            icons: {
                refresh: "glyphicon-repeat",
                toggle: "glyphicon-list-alt",
                columns: "glyphicon-list"
            },
		    columns: [
		    {
		        field: 'id',
		        title: 'ID',
		        align: 'center'
		    }, {
		        field: 'username',
		        title: '用户名',
		        align: 'center'  
		    }, {
		        field: 'usercode',
		        title: '用户账号',
		        align: 'center'  
		    }, {
		        field: 'locked',
		        title: '用户状态',
		        align: 'center'  
		    }, {
		        field: 'locked',
		        title: '操作',
		        align: 'center',
		        width: 200,
		        formatter: function (value) {
		        	return '<button type="button" value="'+value+'" class="btn btn-outline btn-primary">修改</button> <button type="button" class="btn btn-outline btn-danger">删除</button>';
		        }
		    } ]
		});
        
		// 点击事件
	</script>

</body>


<!-- Mirrored from www.zi-han.net/theme/hplus/table_basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:20:01 GMT -->
</html>
