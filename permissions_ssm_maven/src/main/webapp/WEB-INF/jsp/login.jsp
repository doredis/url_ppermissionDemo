<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>


<!-- Mirrored from www.zi-han.net/theme/hplus/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:18:23 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>H+ 后台主题UI框架 - 登录</title>
    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

    <link href="${pageContext.request.contextPath}/hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">

    <link href="${pageContext.request.contextPath}/hplus/css/animate.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <script>if(window.top !== window.self){ window.top.location = window.location;}</script>
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen  animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">H+</h1>

            </div>
            <h3>欢迎使用 H+</h3>

            <form id="loginFrom" class="m-t" role="form" method="post" action="${pageContext.request.contextPath}/login">
                <div class="form-group">
                    <input type="text" name="username" class="form-control" placeholder="用户名" required="">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="密码" required="">
                </div>
                
                <div class="form-group" style="float: left">
                		<img id="kaptchaImage" />
                </div>
                
                <div class="form-group">
                	<a href="#" id="refresh">看不清?换一张</a>  
                </div>
                
                <div class="form-group">
                    <input type="text" name="captchaCode" class="form-control" placeholder="验证码" required="">
                </div>
                
                <div class="form-group">
                	记住我：<input type="checkbox" name="rememberMe">
                </div>
                	
                <button type="submit" class="btn btn-primary block full-width m-b">登 录</button>

                <p class="text-muted text-center"> <a href="login.html#"><small>忘记密码了？</small></a> | <a href="${pageContext.request.contextPath}/openRegiste">注册一个新账号</a>
                </p>

            </form>
        </div>
    </div>
    <script src="${pageContext.request.contextPath}/hplus/js/jquery.min.js?v=2.1.4"></script>
    <script src="${pageContext.request.contextPath}/hplus/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="${pageContext.request.contextPath}/hplus/js/plugins/layer/layer.min.js"></script>
    <script type="text/javascript">
    	
    $(function(){
    	
    	// 获取登录状态
    	var message = '${message}';
    	
    	if(message != ''){
    		layer.msg(message, {time : 1000}, function () {
    			window.location.href='${pageContext.request.contextPath}/login';
    		});
    	}
    	
    	// 加载验证码
    	 $("#kaptchaImage").hide().attr('src', '${pageContext.request.contextPath}/getCaptcha?' + Math.floor(Math.random()*100) ).fadeIn();
    	
     	$("#refresh, #kaptchaImage").click(function() {
    		$("#kaptchaImage").hide().attr('src', '${pageContext.request.contextPath}/getCaptcha?' + Math.floor(Math.random()*100) ).fadeIn(); 
    	}); 
    });
    </script>
</body>


<!-- Mirrored from www.zi-han.net/theme/hplus/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:18:23 GMT -->
</html>
