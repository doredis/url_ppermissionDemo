package com.ht.mapper;

import java.util.List;

import com.ht.model.Permission;

/**
 * 
 * 菜单、权限列表查询
 * @author Administrator
 *
 */
public interface PermissionMapperCustom {
	
	// 通过用户 id 查询菜单
	public List<Permission> getMenus(Integer id);
	
	// 查询用户 id 权限
	public List<Permission> getPermissions(Integer id);
}