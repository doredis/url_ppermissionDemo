package com.ht.mapper;

import com.ht.model.ExceptionLog;
import com.ht.model.ExceptionLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ExceptionLogMapper {
    long countByExample(ExceptionLogExample example);

    int deleteByExample(ExceptionLogExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ExceptionLog record);

    int insertSelective(ExceptionLog record);

    List<ExceptionLog> selectByExampleWithBLOBs(ExceptionLogExample example);

    List<ExceptionLog> selectByExample(ExceptionLogExample example);

    ExceptionLog selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ExceptionLog record, @Param("example") ExceptionLogExample example);

    int updateByExampleWithBLOBs(@Param("record") ExceptionLog record, @Param("example") ExceptionLogExample example);

    int updateByExample(@Param("record") ExceptionLog record, @Param("example") ExceptionLogExample example);

    int updateByPrimaryKeySelective(ExceptionLog record);

    int updateByPrimaryKeyWithBLOBs(ExceptionLog record);

    int updateByPrimaryKey(ExceptionLog record);
}