package com.ht.handler;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import com.ht.mapper.ExceptionLogMapper;
import com.ht.model.ExceptionLog;
import com.ht.util.IPUtil;

public class GlobalHandler implements HandlerExceptionResolver {

	@Autowired
	private ExceptionLogMapper exceptionLogMapper;
	
	private final static Logger logger = Logger.getLogger(GlobalHandler.class);
	
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		
		logger.info("系统出现异常  >>>>>>");
		logger.info("访问地址  >>>>>>  " + request.getRequestURI());
		logger.info("访问参数  >>>>>>  " + request.getQueryString());
		
		ex.printStackTrace();
		
		// 异常持久化
		exceptionSave(request, handler, ex);
		
		// 保存异常信息到session
		request.setAttribute("message", ex.getMessage());
		
		logger.info("该异常已持久化到数据库  >>>>>>");
		return new ModelAndView();
	}
	
	// 异常持久化
	private void exceptionSave(HttpServletRequest request, Object handler, Exception ex) {

		String ip = IPUtil.getIpAddrByRequest(request);
        HandlerMethod handlerMethod = (HandlerMethod) handler;  
        String className = handlerMethod.getBeanType().getName();  
        String methodName = handlerMethod.getMethod().getName();  
        StringWriter message_console = new StringWriter();  
        ex.printStackTrace(new PrintWriter(message_console, true));  
		
        // 异常持久化
        ExceptionLog log = new ExceptionLog();
        log.setIp(ip);
        log.setClassName(className);
        log.setMethodName(methodName);
        log.setExceptionType(ex.getClass().getTypeName());
        log.setExceptionMessage(message_console.toString());
        log.setCreateTime(new Date());
        log.setIsView("0");
        
        exceptionLogMapper.insert(log);
	}
}
