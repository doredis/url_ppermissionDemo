package com.ht.shiro;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;

import com.google.code.kaptcha.Constants;

/**
 * 
 * 自定义验证
 * @author Administrator
 *
 */
public class CustomFormAuthenticationFilter extends FormAuthenticationFilter {

	// 在 shiro 进行验证前进行验证码校验
	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
		
		HttpServletRequest httpServletRequest = (HttpServletRequest)request;
		HttpSession session = httpServletRequest.getSession();
		
		// 获取生成的验证码
		String capText = (String)session.getAttribute(Constants.KAPTCHA_SESSION_KEY);
		// 获取用户输入的验证码
		String captchaCode = httpServletRequest.getParameter("captchaCode");
		
		// 验证码不一致
		if(captchaCode!=null && capText!= null && !captchaCode.equals(capText)) {
			httpServletRequest.setAttribute("message", "验证码错误");
			return true;
		}
		
		// 放行
		return super.onAccessDenied(request, response);
	}
}
