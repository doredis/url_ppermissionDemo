package com.ht.shiro;

import java.util.ArrayList;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import com.ht.model.Permission;
import com.ht.model.User;
import com.ht.service.ILoginService;

/**
 * 
 * 自定义 realm
 * 
 * @author Administrator
 *
 */
public class LoginReam extends AuthorizingRealm {

	// 用户登录服务接口
	@Autowired
	private ILoginService loginService;

	// 认证
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		// 获取用户信息
		String usercode = (String) token.getPrincipal();
	
		// 通过用户账号查询用户
		User user = loginService.getUserByUserCode(usercode);
		if(user == null ) {
			return null;
		}
		
		List<Permission> menus = loginService.getMenus(user.getId());
		user.setMenus(menus);
		
		SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(user, user.getPassword(), ByteSource.Util.bytes(user.getSalt()), "Login");

		return simpleAuthenticationInfo;
	}

	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		// 获取用户主体
		User user = (User)principals.getPrimaryPrincipal();

		// 查询权限
		List<Permission> permissions = loginService.getPermissions(user.getId());

		List<String> permissionsList = new ArrayList<String>();
		
		if(permissions != null) {
			for(Permission per : permissions) {
				permissionsList.add(per.getPercode());
			}
		}
		
		SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
		
		simpleAuthorizationInfo.addStringPermissions(permissionsList);
		
		return simpleAuthorizationInfo;
	}

	//清除缓存
	public void clearCached() {
		PrincipalCollection principals = SecurityUtils.getSubject().getPrincipals();
		super.clearCache(principals);
	}
}
