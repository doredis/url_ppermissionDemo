package com.ht.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.ht.model.Permission;
import com.ht.model.User;
import com.ht.util.ResourcesUtil;

/**
 * 
 * 登录拦截器
 * @author Administrator
 *
 */
public class PermissionInterceptor implements HandlerInterceptor{

	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		String url = request.getRequestURI();
		System.out.println("本次请求URL>>>>>>>" + url);
		
		// 获取匿名URL
		List<String> anonymouslist = ResourcesUtil.gekeyList("anonymousURL");
		for(String anonymous_url : anonymouslist) {
			if(url.indexOf(anonymous_url) >= 0) {
				return true;
			}
		}
		
		User user = (User)request.getSession().getAttribute("user");
		
		if(user != null) {
			// 公共地址
			List<String> commonList = ResourcesUtil.gekeyList("commonURL");
			for(String common_url : commonList) {
				if(url.indexOf(common_url) >= 0) {
					return true;
				}
			}
			
			// 权限列表
			List<Permission> permissions = user.getPermissions();
			for(Permission p : permissions) {
				String permissions_url = p.getUrl();
				System.out.println("当前用户拥有的权限 url" + permissions_url);
				if(url.indexOf(permissions_url) >= 0) {
					return true;
				}
			}
			// 无权访问页面
			request.getRequestDispatcher("/refuse.jsp").forward(request, response);
			return false;
		}
		
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return false;
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
	}

	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		
	}
}
