package com.ht.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * 系统日志拦截器
 * @author Administrator
 *
 */
public class SysLogInterceptor implements HandlerInterceptor{

	// 获取开始时间  
	private long startTime ;
	// 获取结束时间  
	private long endTime ;
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		// 获取开始时间
		startTime = System.currentTimeMillis();  
		return true;
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
	}

	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		endTime=System.currentTimeMillis(); // 获取结束时间  
		System.out.println("本次请求耗时：>>>"+(endTime - startTime)+"ms（毫秒）");
		System.out.println("======================请求完毕========================");
		System.out.println();
	}
}
