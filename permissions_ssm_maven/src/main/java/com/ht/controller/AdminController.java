package com.ht.controller;

import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ht.model.User;
import com.ht.shiro.LoginReam;

/**
 * 
 * 管理操作
 * @author Administrator
 *
 */
@Controller
public class AdminController {

	@Autowired
	private LoginReam loginReam;
	
	// 打开首页
	@RequestMapping("openIndex")
	public String openIndex(Model model) {
		
		Subject subject = SecurityUtils.getSubject();
		User user = (User) subject.getPrincipal();
		model.addAttribute("user", user);
		
		return "index";
	}
	
	// 打开无权访问
	@RequestMapping("openRefuse")
	public String openRefuse() {
		return "refuse";
	}
	
	// 用户退出
	@RequestMapping("exit")
	public String exit(HttpSession session) {
        session.invalidate();
		return "redirect:openLogin";
	}
	
	// 清除缓存
	@RequestMapping("cleanCache")
	public void cleanCache() {
		System.out.println("cleanCache....");
		loginReam.clearCached();
	}
}
