package com.ht.controller;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ht.model.User;
import com.ht.service.IUserService;
import com.ht.util.BootstrapTable;

/**
 * 
 * 用户管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("user")
public class UserController {

	@Autowired
	private IUserService userService;
	
	// 打开用户管理页面
	@RequestMapping("openUser")
	public String openUser() {
		return "user_list";
	}
	
	
	// 用户列表
	@RequestMapping("list")
	@ResponseBody
	public BootstrapTable listUser(BootstrapTable bootstrapTable) {
		System.out.println(bootstrapTable);
		
		PageHelper.offsetPage(bootstrapTable.getStart(), bootstrapTable.getCount());
		List<User> list = userService.list();
		int total = (int) new PageInfo<User>(list).getTotal();
		
		BootstrapTable table = new BootstrapTable();
		table.setRows(list);
		table.setTotal(total);
		
		return table;
	}
}
