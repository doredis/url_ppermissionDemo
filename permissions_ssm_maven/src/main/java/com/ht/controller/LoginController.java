package com.ht.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ht.service.ILoginService;

/**
 * 
 * 登录
 * 
 * @author Administrator
 *
 */
@Controller
public class LoginController {

	@Autowired
	private ILoginService loginService;

	// 打开登录页面
	@RequestMapping("openLogin")
	public String openLogin() {
		System.out.println("打开页面");
		return "login";
	}

	/*
	 * 登录状态码
	 * -1：账号或密码错误
	 * 0：验证码错误
	 * */
	/**
	 * @param captchaCode： 验证码
	 * @throws Exception
	 */
	@RequestMapping("login")
	public String login(Model model, HttpServletRequest request) throws Exception {
		//String capText = (String)session.getAttribute(Constants.KAPTCHA_SESSION_KEY);
		//如果登陆失败从request中获取认证异常信息，shiroLoginFailure就是shiro异常类的全限定名
		String exceptionClassName = (String) request.getAttribute("shiroLoginFailure");
		//根据shiro返回的异常类路径判断，抛出指定异常信息
		if(exceptionClassName!=null){
			if (UnknownAccountException.class.getName().equals(exceptionClassName)) {
				//最终会抛给异常处理器
				model.addAttribute("message", "账号不存在");
			} else if (IncorrectCredentialsException.class.getName().equals(
					exceptionClassName)) {
				model.addAttribute("message", "用户名/密码错误");
			}else {
				model.addAttribute("message", "未知错误");
				throw new Exception();//最终在异常处理器生成未知错误
			}
		}
		
		//此方法不处理登陆成功（认证成功），shiro认证成功会自动跳转到上一个请求路径
		
		return "login";
	}
}
