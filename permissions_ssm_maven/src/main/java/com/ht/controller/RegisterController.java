package com.ht.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ht.model.User;
import com.ht.service.IRegisterService;

/**
 * 
 * 用户注册
 * 
 * @author Administrator
 *
 */
@Controller
public class RegisterController {

	@Autowired
	private IRegisterService registerService;
	
	// 打开注册页面
	@RequestMapping("openRegiste")
	public String openRegiste() {
		
		return "register";
	}
	
	// 用户注册
	@RequestMapping("save")
	public String save(User user) throws Exception {
		
		registerService.save(user);
		
		return "login";
	}
}
