package com.ht.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ht.mapper.UserMapper;
import com.ht.model.User;
import com.ht.service.IRegisterService;
import com.ht.util.MD5;

/**
 * 
 * 用户注册服务
 * @author Administrator
 *
 */
@Service
public class RegisterServiceImpl implements IRegisterService {

	@Autowired
	private UserMapper userMapper;
	
	public void save(User user) {
		// 对用户密码进行加密
		String md5ofStr = new MD5().getMD5ofStr(user.getPassword());
		user.setPassword(md5ofStr);
		user.setLocked("1");
		userMapper.insert(user);
	}
	
}
