package com.ht.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ht.mapper.PermissionMapperCustom;
import com.ht.mapper.UserMapper;
import com.ht.model.Permission;
import com.ht.model.User;
import com.ht.model.UserExample;
import com.ht.service.ILoginService;
import com.ht.util.MD5;


@Service
public class LoginServiceImpl implements ILoginService {

	@Autowired
	private UserMapper userMapper;
	
	// 自定义权限查询
	@Autowired
	private PermissionMapperCustom permissionMapperCustom;

	public User login(User user) {
		
		User user_db = getUserByUserCode(user.getUsercode());
		
		if(user_db == null) {
			return null;
		}
		
		// 加密网页输入
		String password_input_md5 = new MD5().getMD5ofStr(user.getPassword());
		
		if(!password_input_md5.equalsIgnoreCase(user_db.getPassword())) {
			return null;
		}
		
		// 授权
		List<Permission> menus = getMenus(user_db.getId());
		List<Permission> permissions = getPermissions(user_db.getId());

		user_db.setMenus(menus);
		user_db.setPermissions(permissions);
		
		return user_db;
	}

	// 通过用户账号查询用户是否存在
	public User getUserByUserCode(String usercode) {

		UserExample userExample = new UserExample();

		UserExample.Criteria cr = userExample.createCriteria();

		cr.andUsercodeEqualTo(usercode);

		List<User> list = userMapper.selectByExample(userExample);

		if (list != null && list.size() == 1) {
			return list.get(0);
		}
		return null;
	}
	
	// 通过用户 id 查询菜单
	public List<Permission> getMenus(Integer id) {
		return permissionMapperCustom.getMenus(id);
	}
	
	// 查询用户 id 权限
	public List<Permission> getPermissions(Integer id) {
		return permissionMapperCustom.getPermissions(id);
	}
}
