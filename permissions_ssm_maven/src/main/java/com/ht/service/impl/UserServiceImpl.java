package com.ht.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ht.mapper.UserMapper;
import com.ht.model.User;
import com.ht.model.UserExample;
import com.ht.service.IUserService;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private UserMapper userMapper;
	
	// 自定义条件查询对象
	private UserExample userExample = new UserExample();
	
	public List<User> list() {
		
		UserExample.Criteria cr = userExample.createCriteria();
		
		List<User> list = userMapper.selectByExample(userExample);
		
		return list;
	}
}
