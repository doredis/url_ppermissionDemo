package com.ht.service;

import java.util.List;

import com.ht.model.Permission;
import com.ht.model.User;

/**
 * 
 * 用户登录
 * @author Administrator
 *
 */
public interface ILoginService {
	
	// 登录
	User login(User user);
	
	// 通过用户账号查询用户是否存在
	User getUserByUserCode(String usercode);
	
	// 通过用户 id 查询菜单
	public List<Permission> getMenus(Integer id);
	
	// 查询用户 id 权限
	public List<Permission> getPermissions(Integer id);
}