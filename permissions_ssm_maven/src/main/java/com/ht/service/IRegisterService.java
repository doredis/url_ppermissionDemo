package com.ht.service;

import com.ht.model.User;

/**
 * 
 * 用户注册
 * @author Administrator
 *
 */
public interface IRegisterService {
	
	// 注册用户
	void save(User user);
}
