package com.ht.service;

import java.util.List;

import com.ht.model.User;

/**
 * 
 * 用户服务接口
 * @author Administrator
 *
 */
public interface IUserService {
	
	
	List<User> list();
}
