package com.ht.util;

import java.util.List;

public class BootstrapTable {

	// sort: "0", order: "asc", limit: 10, offset: 0

	// 从第几条开始
	private Integer start;
	// 查多少条
	private Integer count;
	// 总数量
	private Integer total;
	private List<Object> rows;
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public List getRows() {
		return rows;
	}
	public void setRows(List rows) {
		this.rows = rows;
	}
	@Override
	public String toString() {
		return "BootstrapTable [start=" + start + ", count=" + count + ", total=" + total + ", rows=" + rows + "]";
	}
	
}