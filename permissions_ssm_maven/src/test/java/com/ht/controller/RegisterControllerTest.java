package com.ht.controller;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ht.model.User;
import com.ht.service.IRegisterService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:application.xml")
public class RegisterControllerTest {

	@Autowired
	private IRegisterService registerService;
	
	@Test
	public void testSave() {
		
		for(int i=0; i <=100; i++) {
			User user = new User();
			user.setUsername("test" + i);
			user.setUsercode("test" + i);
			user.setPassword("test" + i);
			registerService.save(user);
		}
		
	}
}
