package com.ht.mapper;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ht.model.User;
import com.ht.model.UserExample;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:application.xml")
public class UserMapperTest {

	@Autowired
	private UserMapper userMapper;
	
	@Test
	public void testUpdateByExampleSelective() {
		
		UserExample userExample = new UserExample();
		
		UserExample.Criteria cr = userExample.createCriteria();
		
		List<User> list = userMapper.selectByExample(userExample);
		
		for(User u : list) {
			System.out.println(u.getUsercode());
		}
		
	}
}
